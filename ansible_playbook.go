package ansible

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"
)

var logger = log.Default()

func init() {
	logger.SetFlags(log.Llongfile)
}

type Envs = map[string]string

type Playbook struct {
	dir       string
	cmd       *exec.Cmd
	timeout   int
	Path      string
	Stdout    io.Writer
	Stdin     io.Reader
	Stderr    io.Writer
	Inventory interface{}
	Args      []string
	ExtraVars map[string]string
	Tags      string
	Envs      Envs
	isPty     bool
	ctx       context.Context
}

func NewPlaybook(binPath string, inventory interface{}, ctx context.Context) *Playbook {
	if binPath == "" {
		binPath = "ansible-playbook"
	}

	if ctx == nil {
		ctx = context.Background()
	}

	//ctx, _ = context.WithCancel(ctx)
	return &Playbook{
		timeout:   600,
		Path:      binPath,
		Inventory: inventory,
		ExtraVars: map[string]string{},
		Args:      []string{},
		ctx:       ctx,
	}
}

func (ap *Playbook) WorkDirectory(dir string) *Playbook {
	ap.dir = dir
	return ap
}

func (ap *Playbook) RequestPty() *Playbook {

	ap.isPty = true
	return ap
}

func (ap *Playbook) SetArgs(args ...string) *Playbook {
	ap.Args = append(ap.Args, args...)
	return ap
}

func (ap *Playbook) SetEnv(k, v string) *Playbook {
	if ap.Envs == nil {
		ap.Envs = Envs{}
	}
	ap.Envs[k] = v
	return ap
}

func (ap *Playbook) SetEnvs(e Envs) *Playbook {
	for k, v := range e {
		ap.SetEnv(k, v)
	}
	return ap
}

func (ap *Playbook) SetAdditionalVar(k, v string) *Playbook {
	ap.ExtraVars[k] = v
	return ap
}
func (ap *Playbook) SetAdditionalVars(e map[string]string) *Playbook {
	for k, v := range e {
		ap.SetAdditionalVar(k, v)
	}
	return ap
}

func (ap *Playbook) SetTags(tags string) *Playbook {
	ap.Tags = tags
	return ap
}

func (ap *Playbook) SetOutJson() *Playbook {

	return ap.SetEnvs(Envs{
		"ANSIBLE_STDOUT_CALLBACK":       "json",
		"ANSIBLE_LOAD_CALLBACK_PLUGINS": "True",
	})
}
func (ap *Playbook) SetOutTimer() *Playbook {

	return ap.SetEnvs(Envs{
		"ANSIBLE_STDOUT_CALLBACK":       "timer",
		"ANSIBLE_LOAD_CALLBACK_PLUGINS": "True",
	})
}

func (ap *Playbook) ForceColor() *Playbook {

	return ap.SetEnvs(Envs{
		"ANSIBLE_FORCE_COLOR": "True",
	})
}

func (ap *Playbook) CloseWarnings() *Playbook {

	return ap.SetEnvs(Envs{
		"ANSIBLE_DEPRECATION_WARNINGS":       "false",
		"ANSIBLE_ACTION_WARNINGS":            "false",
		"ANSIBLE_LOCALHOST_WARNING":          "false",
		"ANSIBLE_INVENTORY_UNPARSED_WARNING": "false",
	})
}

func (ap *Playbook) RunWithFile(playbookFile string) (o []byte, e []byte, err error) {
	stdout := bytes.Buffer{}
	stderr := bytes.Buffer{}

	if strings.HasPrefix(playbookFile, "/") {
		_, err = os.Stat(playbookFile)
		if err != nil {
			return nil, nil, err
		}
	} else {
		_, err = os.Stat(path.Join(ap.dir, playbookFile))
		if err != nil {
			return nil, nil, err
		}
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Second*600)

	cmd := exec.CommandContext(ctx, ap.Path)
	cmd.Dir = ap.dir
	ap.cmd = cmd
	cmd.Args = append(cmd.Args, ap.Args...)

	switch ap.Inventory.(type) {
	case string:
		s, _ := ap.Inventory.(string)
		if s != "" {
			cmd.Args = append(cmd.Args, "-i", s)
		}
	case *Inventory, Inventory:
		i, _ := ap.Inventory.(*Inventory)
		tempFile, err := i.LocalStorage()
		if err != nil {
			return nil, nil, err
		}
		defer i.Close()
		cmd.Args = append(cmd.Args, "-i", tempFile)
	default:
		panic("inventory type error")
	}

	cmd.Args = append(cmd.Args, playbookFile)

	if ap.Envs != nil {
		for k, v := range ap.Envs {
			ap.cmd.Env = append(ap.cmd.Environ(), fmt.Sprintf("%v=%v", k, v))
		}
	}

	if ap.ExtraVars != nil {
		for k, v := range ap.ExtraVars {
			ap.cmd.Args = append(ap.cmd.Args, "-e", strings.Join([]string{k, v}, "="))
		}
	}

	if ap.Tags != "" {
		ap.cmd.Args = append(ap.cmd.Args, "-t", ap.Tags)
	}

	//if !ap.isPty {
	if ap.Stdout == nil {
		ap.cmd.Stdout = &stdout
	} else {
		ap.cmd.Stdout = ap.Stdout
	}

	if ap.Stdin != nil {
		ap.cmd.Stdin = ap.Stdin
	}

	if ap.Stderr == nil {
		ap.cmd.Stderr = &stderr
	} else {
		ap.cmd.Stderr = ap.Stderr
	}
	err = cmd.Start()
	if err != nil {
		return stdout.Bytes(), stderr.Bytes(), err
	}
	//} else {
	//	ptmx, err := pty.Start(cmd)
	//	if err != nil {
	//		return stdout.Bytes(), stderr.Bytes(), err
	//	}
	//	defer ptmx.Close()
	//	if ap.Stdout != nil {
	//		_, err = io.Copy(ap.Stdout, ptmx)
	//		if err != nil {
	//			return nil, nil, err
	//		}
	//	} else {
	//		_, err = io.Copy(&stdout, ptmx)
	//		if err != nil {
	//			return stdout.Bytes(), nil, err
	//		}
	//	}
	//
	//}
	//fmt.Println(cmd.ProcessState.Exited(), cmd.ProcessState.Sys())
	go func() {
		select {
		case <-ctx.Done():
			return
		case <-ap.ctx.Done():
			if ap.cmd.ProcessState == nil {
				err := ap.cmd.Process.Kill()
				if err != nil {
					logger.Println(err)

				}
			}
		case <-time.After(time.Second * 600):
			return
		}
	}()
	err = ap.cmd.Wait()
	return stdout.Bytes(), stderr.Bytes(), err
}

func (ap *Playbook) RunWithBytes(playbook []byte) (o []byte, e []byte, err error) {

	tempF, err := os.CreateTemp("", "ansible_playbook_*.yaml")
	if err != nil {
		return nil, nil, err
	}
	defer os.Remove(tempF.Name())
	_, err = io.Copy(tempF, bytes.NewBuffer(playbook))
	if err != nil {
		return nil, nil, err

	}
	return ap.RunWithFile(tempF.Name())
}
