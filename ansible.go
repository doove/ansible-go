package ansible

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"strings"
	"time"
)

type Ansible struct {
	dir       string
	cmd       *exec.Cmd
	Args      []string
	timeout   int
	Stdout    io.Writer
	Stdin     io.Reader
	Stderr    io.Writer
	Path      string
	Inventory interface{}
	HostList  []string
	ExtraVars map[string]string
	Envs      Envs
	isPty     bool
	ctx       context.Context
}

func (a *Ansible) String() string {
	return fmt.Sprintf("%+q", a.cmd.Args)
}

func (a *Ansible) WorkDirectory(dir string) *Ansible {
	a.dir = dir
	return a
}

func (a *Ansible) RequestPty() *Ansible {

	a.isPty = true
	return a
}

func (a *Ansible) RunWithModule(module string, moduleArgs ...string) (o []byte, e []byte, err error) {
	var timeout time.Duration
	stdout := bytes.Buffer{}
	stderr := bytes.Buffer{}
	if a.timeout == 0 {
		timeout = 600 * time.Second
	} else {
		timeout = time.Duration(a.timeout) * time.Second
	}
	ctx, _ := context.WithTimeout(context.Background(), timeout)
	cmd := exec.CommandContext(ctx, a.Path)
	cmd.Dir = a.dir
	a.cmd = cmd
	cmd.Args = append(cmd.Args, a.Args...)
	switch a.Inventory.(type) {
	case string:
		s, _ := a.Inventory.(string)
		if s != "" {
			cmd.Args = append(cmd.Args, "-i", s)
		}
	case *Inventory, Inventory:
		i, _ := a.Inventory.(*Inventory)
		tempFile, err := i.LocalStorage()
		if err != nil {
			return nil, nil, err
		}
		defer i.Close()
		cmd.Args = append(cmd.Args, "-i", tempFile)
	default:
		panic("inventory type error")
	}

	cmd.Args = append(cmd.Args, strings.Join(a.HostList, ","))
	if module == "" {
		return nil, nil, errors.New("module is empty")
	}
	cmd.Args = append(cmd.Args, "-m", module)
	if len(moduleArgs) != 0 {
		cmd.Args = append(cmd.Args, "-a")
		cmd.Args = append(cmd.Args, moduleArgs...)
	}

	if a.Envs != nil {
		for k, v := range a.Envs {
			a.cmd.Env = append(a.cmd.Environ(), fmt.Sprintf("%v=%v", k, v))
		}
	}

	if a.ExtraVars != nil {
		for k, v := range a.ExtraVars {
			a.cmd.Args = append(a.cmd.Args, "-e", strings.Join([]string{k, v}, "="))
		}
	}

	//if !a.isPty {
	if a.Stdout == nil {
		a.cmd.Stdout = &stdout
	} else {
		a.cmd.Stdout = a.Stdout
	}

	if a.Stdin != nil {
		a.cmd.Stdin = a.Stdin
	}

	if a.Stderr == nil {
		a.cmd.Stderr = &stderr
	} else {
		a.cmd.Stderr = a.Stderr
	}

	err = cmd.Start()
	if err != nil {
		return stdout.Bytes(), stderr.Bytes(), err
	}
	//} else {
	//	ptmx, err := pty.Start(cmd)
	//	if err != nil {
	//		return stdout.Bytes(), stderr.Bytes(), err
	//	}
	//	defer ptmx.Close()
	//	if a.Stdout != nil {
	//		_, err = io.Copy(a.Stdout, ptmx)
	//		if err != nil {
	//			return nil, nil, err
	//		}
	//	} else {
	//		_, err = io.Copy(&stdout, ptmx)
	//		if err != nil {
	//			return stdout.Bytes(), nil, err
	//		}
	//	}
	//
	//}
	go func() {
		select {
		case <-ctx.Done():
		case <-a.ctx.Done():
			if !a.cmd.ProcessState.Exited() {
				err := a.cmd.Process.Kill()
				if err != nil {
					logger.Println(err)
				}
			}
		case <-time.After(time.Second * 600):

		}
	}()
	err = cmd.Wait()
	return stdout.Bytes(), stderr.Bytes(), err
}

func (a *Ansible) SetArgs(args ...string) *Ansible {
	a.Args = append(a.Args, args...)
	return a
}

func (a *Ansible) Shell(moduleArgs string) (o []byte, e []byte, err error) {
	return a.RunWithModule("shell", moduleArgs)
}

func (a *Ansible) Script(moduleArgs string) (o []byte, e []byte, err error) {
	return a.RunWithModule("script", moduleArgs)
}

func (a *Ansible) Command(moduleArgs string) (o []byte, e []byte, err error) {
	return a.RunWithModule("command", moduleArgs)
}

func (a *Ansible) Ping() (o []byte, e []byte, err error) {
	return a.RunWithModule("ping")
}

func (a *Ansible) SetAdditionalVar(k, v string) *Ansible {
	a.ExtraVars[k] = v
	return a
}
func (a *Ansible) SetAdditionalVars(e map[string]string) *Ansible {
	for k, v := range e {
		a.SetAdditionalVar(k, v)
	}
	return a
}

func (a *Ansible) SetEnv(k, v string) *Ansible {
	if a.Envs == nil {
		a.Envs = Envs{}
	}
	a.Envs[k] = v
	return a
}

func (a *Ansible) SetEnvs(e Envs) *Ansible {
	for k, v := range e {
		a.SetEnv(k, v)
	}
	return a
}

func (a *Ansible) SetOutJson() *Ansible {

	return a.SetEnvs(Envs{
		"ANSIBLE_STDOUT_CALLBACK":       "json",
		"ANSIBLE_LOAD_CALLBACK_PLUGINS": "True",
	})
}

func (a *Ansible) ForceColor() *Ansible {

	return a.SetEnvs(Envs{
		"ANSIBLE_FORCE_COLOR": "True",
	})
}

func (a *Ansible) CloseWarnings() *Ansible {

	return a.SetEnvs(Envs{
		"ANSIBLE_DEPRECATION_WARNINGS":       "false",
		"ANSIBLE_ACTION_WARNINGS":            "false",
		"ANSIBLE_LOCALHOST_WARNING":          "false",
		"ANSIBLE_INVENTORY_UNPARSED_WARNING": "false",
	})
}

func NewAnsible(binPath string, inventory interface{}, hosts []string, ctx context.Context) (ansible *Ansible) {
	if binPath == "" {
		binPath = "ansible"
	}
	if len(hosts) == 0 || hosts == nil {
		hosts = []string{"localhost"}
	}

	if ctx == nil {
		ctx = context.Background()
	}
	//ctx, _ = context.WithCancel(ctx)

	return &Ansible{
		timeout:   600,
		Path:      binPath,
		Inventory: inventory,
		HostList:  hosts,
		ExtraVars: map[string]string{},
		Envs:      Envs{},
		ctx:       ctx,
	}
}
