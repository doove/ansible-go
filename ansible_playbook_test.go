package ansible

import (
	"testing"
)

var playbook = NewPlaybook("", inventory)

func TestRunn(t *testing.T) {
	t.Log("Running")
}

func TestPlaybook_RunWithFile(t *testing.T) {
	output, _, err := playbook.
		//SetOutJson().
		CloseWarnings().
		RequestPty().
		RunWithFile("test.yaml")
	if err != nil {
		t.Error(err)
	}
	t.Log(string(output))

}

func TestPlaybook_RunWithBytes(t *testing.T) {
	output, _, err := playbook.
		SetOutJson().
		CloseWarnings().
		RunWithBytes([]byte(`---
- hosts: localhost
  tasks:
  - name: test
    setup:
    register: ss
  - name: debug
    debug:
      var: ss`))
	if err != nil {
		t.Error(string(output))
		t.Error(err)
	}
}
