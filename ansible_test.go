package ansible

import (
	"os/exec"
	"testing"
)

var inventory = NewInventory()

var _ = func() (_ struct{}) {
	inventory.AddHost("10.211.55.14", "root", "root", 22, nil)
	return
}()

var ansible = NewAnsible("/usr/local/bin/ansible", inventory, []string{"all"})

func TestRunWithModule(t *testing.T) {

	output, _, err := ansible.RequestPty().CloseWarnings().RunWithModule("ping")
	if err != nil {
		t.Log(ansible)
		t.Error(string(output))
		t.Fatal(err)
		return
	}
	t.Log(ansible)

	t.Log(string(output))

}

func TestNewAnsible(t *testing.T) {
	cmd := exec.Command("/bin/ls", "/")
	out, err := cmd.CombinedOutput()
	if err != nil {
		t.Log(cmd)
		t.Fatal(string(out), err)
	}
}

func TestAnsible_Shell(t *testing.T) {
	output, _, err := ansible.CloseWarnings().RequestPty().SetOutJson().Shell("/usr/bin/whereis ansible")
	if err != nil {
		t.Log(ansible)
		t.Error(string(output))
		t.Error(err)
	}
	t.Log(ansible)

	t.Log(string(output))

}

func TestAnsible_Ping(t *testing.T) {
	output, _, err := ansible.CloseWarnings().SetOutJson().Ping()
	if err != nil {
		t.Error(err)
	}
	t.Log(string(output))

}

func TestAnsible_Cmd(t *testing.T) {
	output, _, err := ansible.Command("ls /")
	if err != nil {
		t.Error(string(output))
		t.Error(err)
	}
}
