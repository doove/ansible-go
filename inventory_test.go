package ansible

import (
	"testing"
)

func Test_host_String(t *testing.T) {
	host := newHost("localhost", "root", "root", 22, map[string]string{
		"a": "a",
		"b": "b",
		"c": "c",
	})
	t.Log(host)
	return
}

func TestNewInventory(t *testing.T) {
	inventory := NewInventory()
	inventory.AddHost("localhost", "root", "root", 22, nil)
	inventory.AddHost("localhost2", "root", "root", 22, map[string]string{
		"a": "a",
		"b": "b",
		"c": "c",
	})
	t.Log(inventory)
	return
}
